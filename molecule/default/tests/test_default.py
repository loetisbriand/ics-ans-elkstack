import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_filebeat_running_and_enabled(host):
    filebeat = host.service("filebeat")
    assert filebeat.is_running
    assert filebeat.is_enabled


def test_logstash_running_and_enabled(host):
    logstash = host.service("logstash")
    assert logstash.is_running
    assert logstash.is_enabled


def test_elasticsearch_running_and_enabled(host):
    elasticsearch = host.service("elasticsearch")
    assert elasticsearch.is_running
    assert elasticsearch.is_enabled


def test_kibana_running_and_enabled(host):
    kibana = host.service("kibana")
    assert kibana.is_running
    assert kibana.is_enabled
